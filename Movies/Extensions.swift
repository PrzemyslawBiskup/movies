//
//  Extensions.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

extension Data {
    
    var json: Any? { try? JSONSerialization.jsonObject(with: self, options: []) }
    
    var utf8: String? { String(data: self, encoding: .utf8) }
    
    func decodeFor<T: Decodable>(_ type: T.Type) -> T? {
        do {
            return try AppDecoder().decode(T.self, from: self)
        } catch(let error) {
            print(error, Date())
            return nil
        }
    }
}

extension Double {
    
    var stringRepresentation: String {
        let value = Double(Int(self*10))/10.0
        return "\(value)"
    }
}

extension NSNotification.Name {
    
    static let addToFavorites = NSNotification.Name("addToFavorites")
    
    static let removeFromFavorite = NSNotification.Name("removeFromFavorite")
}
