//
//  SearchResultsPresenter.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 25/10/2021.
//

import Foundation

protocol SearchResultsView: AnyObject {
    func reloadMovies()
}

final class SearchResultsPresenter: NSObject {
    
    init(view: SearchResultsView) {
        super.init()
        self.view = view
        NotificationCenter.default.addObserver(forName: .removeFromFavorite, object: nil, queue: nil) { [weak self] notification in
            guard let id = notification.object as? Int,
                  let index = self?.movies.enumerated().first(where: { $0.element.id == id })?.offset else { return }
            self?.movies[index].isFavorite = false
        }
        NotificationCenter.default.addObserver(forName: .addToFavorites, object: nil, queue: nil) { [weak self] notification in
            guard let id = notification.object as? Int,
                  let index = self?.movies.enumerated().first(where: { $0.element.id == id })?.offset else { return }
            self?.movies[index].isFavorite = true
        }
    }
    
    private weak var view: SearchResultsView?
    
    private(set) var movies: [Movie] = [] { didSet { view?.reloadMovies() } }
    
    func searchMovie(text: String) {
        MoviesService().search(text: text) { result in
            switch result {
            case .success(var movies):
                StorageManager.favoriteMovies.forEach { id in
                    if let index = movies.enumerated().first(where: { $0.element.id == id })?.offset {
                        movies[index].isFavorite = true
                    }
                }
                self.movies = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setFavorite(index: Int) {
        let movieId = movies[index].id
        StorageManager.setFavorite(id: movieId)
    }
}
