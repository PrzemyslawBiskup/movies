//
//  AppDelegate.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import UIKit

@main final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = MoviesController()
        window?.makeKeyAndVisible()
        return true
    }
}
