//
//  MovieDetailsController.h
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MovieDetailsController: UIViewController

@property (nonatomic) int movieId;
@property (nonatomic, retain) NSString* movieTitle;
@property (nonatomic, retain) NSString* releaseDate;
@property (nonatomic, retain) NSString* voteAverage;
@property (nonatomic, retain) NSString* overview;
@property (nonatomic, nullable, retain) NSString* path;
@property (nonatomic) bool isFavorite;

- (instancetype)initWithId:(int)movieId
                     title:(NSString*)title
               releaseDate:(NSString*)releaseDate
               voteAverage:(NSString*)voteAverage
                  overview:(NSString*)overview
                      path:(nullable NSString*)path
                isFavorite:(bool)isFavorite;

@end

NS_ASSUME_NONNULL_END
