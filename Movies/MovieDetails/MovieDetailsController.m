//
//  MovieDetailsController.m
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

#import "MovieDetailsController.h"
#import <Movies-Swift.h>

@interface MovieDetailsController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@end

@implementation MovieDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = _movieTitle;
    self.dateLabel.text = _releaseDate;
    self.averageLabel.text = _voteAverage;
    self.overviewLabel.text = _overview;
    [_favoriteButton setTitle:(self.isFavorite == true) ? @"★" : @"✩" forState:UIControlStateNormal];
    ImageService* imageService = [ImageService new];
    [imageService fetchImageWithPath: _path completionHandler:^(UIImage * _Nullable image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = image;
        });
    }];
}

- (instancetype)initWithId:(int)movieId
                     title:(NSString*)title
               releaseDate:(NSString*)releaseDate
               voteAverage:(NSString*)voteAverage
                  overview:(NSString*)overview
                      path:(nullable NSString*)path
                isFavorite:(bool)isFavorite {
    self = [super init];
    if (self) {
        _movieId = movieId;
        _movieTitle = title;
        _releaseDate = releaseDate;
        _voteAverage = voteAverage;
        _overview = overview;
        _isFavorite = isFavorite;
        _path = path;
    }
    return self;
}

- (IBAction)setFavorite:(UIButton *)sender {
    _isFavorite = !_isFavorite;
    [_favoriteButton setTitle:(self.isFavorite == true) ? @"★" : @"✩" forState:UIControlStateNormal];
    [StorageManager setFavoriteWithId: _movieId];
}

@end
