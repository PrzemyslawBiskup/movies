//
//  Models.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

struct PlayingMoviesResponse: Codable {
    let results: [Movie]
}

struct Movie: Codable {
    
    let id: Int
    let title: String
    let releaseDate: String
    let voteAverage: Double
    let overview: String
    let path: String?
    var isFavorite: Bool = false
    
    private enum CodingKeys : String, CodingKey {
        case id
        case title
        case releaseDate = "release_date"
        case voteAverage = "vote_average"
        case overview
        case path = "backdrop_path"
    }
}
