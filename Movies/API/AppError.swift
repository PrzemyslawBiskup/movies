//
//  AppError.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

struct AppError: Error {
    let title: ErrorTitle?
    let message: ErrorMessage?
    let statusCode: Int?
    let data: Data?
    let errorDescription: String?
    
    init(title: ErrorTitle? = nil, message: ErrorMessage? = nil, statusCode: Int? = nil, data: Data? = nil, error: Error? = nil) {
        self.title = title
        self.message = message
        self.statusCode = statusCode
        self.data = data
        self.errorDescription = error?.localizedDescription
    }
}

enum ErrorTitle: String {
    case Unauthorized
    case ResponseError
}

enum ErrorMessage: String {
    case WrongHTTPresponse
    case WrongDataModel
    case TIMEOUT
}
