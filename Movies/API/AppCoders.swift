//
//  AppCoders.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

final class AppEncoder: JSONEncoder {
    override init() {
        super.init()
        self.dateEncodingStrategy = .millisecondsSince1970
    }
}

final class AppDecoder: JSONDecoder {
    override init() {
        super.init()
        self.dateDecodingStrategy = .millisecondsSince1970
    }
}
