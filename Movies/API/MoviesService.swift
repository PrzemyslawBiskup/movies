//
//  MoviesService.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

final class MoviesService: RestAPI {
    
    func getMovies(completionHandler: @escaping (Result<[Movie], AppError>) -> Void) {
        request(endpoint: .movie_now_playing, method: .GET) { result in
            switch result {
            case .success(let data):
                if let response = data?.decodeFor(PlayingMoviesResponse.self) {
                    completionHandler(.success(response.results))
                } else {
                    completionHandler(.failure(AppError(title: ErrorTitle.ResponseError,
                                                        message: ErrorMessage.WrongDataModel,
                                                        data: data)))
                }
            case .failure(let error):
                completionHandler(.failure(AppError(error: error)))
            }
        }
    }
    
    func getMovie(id: String, completionHandler: @escaping (Result<Movie, AppError>) -> Void) {
        request(endpoint: .movie(id), method: .GET) { result in
            switch result {
            case .success(let data):
                if let movie = data?.decodeFor(Movie.self) {
                    completionHandler(.success(movie))
                } else {
                    completionHandler(.failure(AppError(title: ErrorTitle.ResponseError,
                                                        message: ErrorMessage.WrongDataModel,
                                                        data: data)))
                }
            case .failure(let error):
                completionHandler(.failure(AppError(error: error)))
            }
        }
    }
    
    func search(text: String, completionHandler: @escaping (Result<[Movie], AppError>) -> Void) {
        request(endpoint: .search, method: .GET, query: [URLQueryItem(name: "query", value: text)]) { result in
            switch result {
            case .success(let data):
                if let response = data?.decodeFor(PlayingMoviesResponse.self) {
                    completionHandler(.success(response.results))
                } else {
                    completionHandler(.failure(AppError(title: ErrorTitle.ResponseError,
                                                        message: ErrorMessage.WrongDataModel,
                                                        data: data)))
                }
            case .failure(let error):
                completionHandler(.failure(AppError(error: error)))
            }
        }
    }
}
