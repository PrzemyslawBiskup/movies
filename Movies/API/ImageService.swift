//
//  ImageService.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 25/10/2021.
//

import Foundation

@objc final class ImageService: NSObject {
    
    private let imageBaseURL = "https://image.tmdb.org/t/p/w300/"
    
    @objc func fetchImage(path: String, completionHandler: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: imageBaseURL + path) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data) else {
                completionHandler(nil)
                return }
            DispatchQueue.main.sync {
                completionHandler(image)
            }
        }.resume()
    }
}
