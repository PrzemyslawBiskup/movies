//
//  RestAPI.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

class RestAPI: NSObject {
    
    private let apiKey = "9e7cde9cf156172c8cf4b832ee969d93"
    
    #if DEBUG
    private let baseURL = "https://api.themoviedb.org/3/"
    #else
    private let baseURL = "https://api.themoviedb.org/3/"
    #endif
    
    enum Method: String {
        case GET
        case POST
        case PATCH
        case DELETE
    }
    
    enum Endpoint {
        case movie_now_playing
        case movie(String)
        case search
        
        var stringValue: String {
            switch self {
            case .movie_now_playing: return "movie/now_playing"
            case .movie(let id): return "movie/\(id)"
            case .search: return "search/movie"
            }
        }
    }
    
    private let headers: [String: String] = ["Content-Type": "application/json",
                                             "X-source" : "IOS",
                                             "Accept": "application/json"]
    
    func request(endpoint: Endpoint, method: Method, query: [URLQueryItem] = [], body: Data? = nil, completionHandler: @escaping (Result<Data?, Error>) -> Void) {
        guard var components = URLComponents(string: baseURL + endpoint.stringValue) else { return }
        components.queryItems = [URLQueryItem(name: "api_key", value: self.apiKey)] + query
        guard let url = components.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        headers.forEach { request.setValue($0.value, forHTTPHeaderField: $0.key) }
        if method == .POST || method == .PATCH { request.httpBody = body }
        
        let dataTask = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                print("🌍🌎💧", Date())
                print(" [REQUEST] ", request.httpMethod ?? "-", request.url ?? "-", "\n",
                    "[HEADERS] ", request.allHTTPHeaderFields ?? [:], "\n",
                    "[BODY] ", request.httpBody?.json ?? request.httpBody?.utf8 ?? request.httpBody ?? "no data", Date(), "\n")
                
                if let error = error {
                    print("[ERROR] ", error.localizedDescription, Date())
                    completionHandler(.failure(AppError(error: error)))
                    return
                }
                guard let response = response as? HTTPURLResponse else {
                    completionHandler(.failure(AppError(title: ErrorTitle.ResponseError,
                                                        message: ErrorMessage.WrongHTTPresponse)))
                    return
                }
                let emoi: String
                switch response.statusCode {
                case 200..<300 :
                    emoi = "☘️🌿🌳"
                    completionHandler(.success(data))
                default:
                    emoi = "🔥🧨💥"
                    completionHandler(.failure(AppError(statusCode: response.statusCode, data: data)))
                }
                print(emoi, Date())
                print(" [CODE] ", response.statusCode, "\n",
                    "[HEADERS] ", response.allHeaderFields as? [String: Any] ?? response.allHeaderFields, "\n",
                    "[BODY] ", data?.json ?? data?.utf8 ?? data ?? "no data", Date(), "\n")
            }
        }
        dataTask.resume()
    }
}
