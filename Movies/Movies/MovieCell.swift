//
//  MovieCell.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 19/10/2021.
//

import UIKit

final class MovieCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var averageLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!
    
    @IBAction private func setFavorite() {
        setFavoriteAction?(self)
    }
    
    var setFavoriteAction: ((MovieCell) -> Void)?
    
    var model: Movie? {
        didSet {
            titleLabel.text = model?.title
            dateLabel.text = model?.releaseDate
            averageLabel.text = model?.voteAverage.stringRepresentation
            favoriteButton.setTitle(model?.isFavorite == true ? "★" : "✩", for: .normal)
        }
    }
}
