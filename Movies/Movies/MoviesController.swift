//
//  MoviesController.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import UIKit

final class MoviesController: UIViewController {
    
    @IBOutlet private weak var searchContainer: UIView!
    @IBOutlet private weak var moviesTableView: UITableView!
    
    private let searchResultController: SearchResultsController = SearchResultsController()
    private lazy var searchController: UISearchController = UISearchController(searchResultsController: searchResultController)
    private lazy var presenter: MoviesPresenter = MoviesPresenter(view: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchContainer.addSubview(searchController.searchBar)
        searchController.searchBar.delegate = searchResultController
        moviesTableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "movieCell")
        presenter.getMovies()
    }
}

extension MoviesController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { presenter.movies.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as! MovieCell
        cell.model = presenter.movies[indexPath.row]
        cell.setFavoriteAction = { [weak self] cell in
            guard let index = tableView.indexPath(for: cell)?.row else { return }
            self?.presenter.setFavorite(index: index)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = presenter.movies[indexPath.row]
        let vc = MovieDetailsController(id: Int32(movie.id),
                                        title: movie.title,
                                        releaseDate: movie.releaseDate,
                                        voteAverage: movie.voteAverage.stringRepresentation,
                                        overview: movie.overview,
                                        path: movie.path,
                                        isFavorite: movie.isFavorite)
        self.present(vc, animated: true, completion: nil)
    }
}

extension MoviesController: MoviesView {
    
    func reloadMovies() { moviesTableView.reloadData() }
}
