//
//  MoviesPresenter.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 18/10/2021.
//

import Foundation

protocol MoviesView: AnyObject {
    func reloadMovies()
}

final class MoviesPresenter: NSObject {
    
    init(view: MoviesView) {
        super.init()
        self.view = view
        NotificationCenter.default.addObserver(forName: .removeFromFavorite, object: nil, queue: nil) { [weak self] notification in
            guard let id = notification.object as? Int,
                  let index = self?.movies.enumerated().first(where: { $0.element.id == id })?.offset else { return }
            self?.movies[index].isFavorite = false
        }
        NotificationCenter.default.addObserver(forName: .addToFavorites, object: nil, queue: nil) { [weak self] notification in
            guard let id = notification.object as? Int,
                  let index = self?.movies.enumerated().first(where: { $0.element.id == id })?.offset else { return }
            self?.movies[index].isFavorite = true
        }
    }
    
    private weak var view: MoviesView?
    
    private(set) var movies: [Movie] = [] { didSet { view?.reloadMovies() } }
    
    func getMovies() {
        MoviesService().getMovies { result in
            switch result {
            case .success(var movies):
                StorageManager.favoriteMovies.forEach { id in
                    if let index = movies.enumerated().first(where: { $0.element.id == id })?.offset {
                        movies[index].isFavorite = true
                    }
                }
                self.movies = movies
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setFavorite(index: Int) {
        let movieId = movies[index].id
        StorageManager.setFavorite(id: movieId)
    }
}
