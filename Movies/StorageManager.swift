//
//  StorageManager.swift
//  Movies
//
//  Created by Przemyslaw Biskup on 19/10/2021.
//

import Foundation

@objc class StorageManager: NSObject {
    
    private static let def = UserDefaults.standard
    
    private(set) static var favoriteMovies: [Int] {
        get { def.value(forKey: "favoriteMovies") as? [Int] ?? [] }
        set { def.setValue(newValue, forKey: "favoriteMovies") }
    }
    
    @objc static func setFavorite(id: Int) {
        if favoriteMovies.contains(id) {
            favoriteMovies.removeAll { $0 == id }
            NotificationCenter.default.post(name: .removeFromFavorite, object: id)
        } else {
            favoriteMovies.append(id)
            NotificationCenter.default.post(name: .addToFavorites, object: id)
        }
    }
}
